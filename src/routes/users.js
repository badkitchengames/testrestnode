const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
  if (req.query.name) {
    res.json({ status: 'ok:' + req.query.name })
  } else {
    res.json({ status: 'ok' })
  }

});

router.get('/:name', (req, res, next) => {
  res.json({ status: 'ok:' + req.params.name })
});


module.exports = router;