const confFile = require('./src/utils/conf')
const express = require('express');
const app = express();


const users = require('./src/routes/users');

app.use((req, res, next) => {
  console.log(`${new Date().toISOString()} => ${req.originalUrl}`);
  next();
});
app.use('/api/users', users);

app.use((req, res, next) => {
  res.status(404).send('Error notning found');
});

app.listen(confFile.env.port, () => {
  console.log('Server started on port: ', confFile.env.port);
});
module.exports = app;